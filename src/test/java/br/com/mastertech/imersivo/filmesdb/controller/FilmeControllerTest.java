package br.com.mastertech.imersivo.filmesdb.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.assertj.core.util.Lists;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.mastertech.imersivo.filmesdb.model.Filme;
import br.com.mastertech.imersivo.filmesdb.service.FilmeService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = { FilmeController.class })

public class FilmeControllerTest {

	@MockBean
	FilmeService filmeService;

	@Autowired
	MockMvc mockMvc;

	@Test
	public void deveObeterListaDeFilmes() throws Exception {
		Filme filme = new Filme();
		filme.setId(1L);
		filme.setGenero("Drama");

		List<Filme> filmes = Lists.newArrayList(filme);

		Mockito.when(filmeService.obterFilmes()).thenReturn(filmes);
		mockMvc.perform(get("/filme")).andExpect(status().isOk())
				.andExpect(content().string(CoreMatchers.containsString("1")))
				.andExpect(content().string(CoreMatchers.containsString("Drama")));
	}

	@Test
	public void deveCriarumFilme() throws JsonProcessingException, Exception {
		Filme filme = new Filme();
		filme.setId(1L);
		filme.setTitulo("Chapolim");

		ObjectMapper objectMapper = new ObjectMapper();

		mockMvc.perform(post("/filme")
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.content(objectMapper.writeValueAsString(filme)))
			.andExpect(MockMvcResultMatchers.status().isCreated());

		Mockito.verify(filmeService).criarFilme(Mockito.any(Filme.class));

	}
	

}

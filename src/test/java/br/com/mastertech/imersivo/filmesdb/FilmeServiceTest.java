package br.com.mastertech.imersivo.filmesdb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import br.com.mastertech.imersivo.filmesdb.model.Filme;
import br.com.mastertech.imersivo.filmesdb.model.FilmeDTO;
import br.com.mastertech.imersivo.filmesdb.repository.FilmeRepository;
import br.com.mastertech.imersivo.filmesdb.service.FilmeService;

@RunWith(SpringRunner.class)
//@SpringBootTest
@ContextConfiguration(classes = { FilmeService.class })

public class FilmeServiceTest {

	@Autowired
	FilmeService filmeService;
	// TODO Auto-generated constructor s

	@MockBean
	FilmeRepository FilmeRepository;

	@Test
	public void deveListarTodosOsFilmes() {

		// FilmeService filmeService = new FilmeService();

		// given

		Filme filme = new Filme();

		List<Filme> filmes = new ArrayList<>();
		filmes.add(filme);

		when(FilmeRepository.findAll()).thenReturn(filmes);

		// action

		Iterable<Filme> resultado = filmeService.obterFilmes();

		// check
		Iterator<Filme> iterador = resultado.iterator();
		Filme filmeResultado = iterador.next();

		// ArrayList<Filme> filmeList = Lists.newArrayList(filmes);

		// assertEquals(1, filmeList.size());

		assertNotNull(filmeResultado);

//		// assertNotNull(filmes);
//		int contador = 0;
//		for (Filme filme : filmes) {
//			contador++;
	}

	@Test
	public void deveListarTodosOsFilmesDeUmDiretor() {
		long id = 1;
		FilmeDTO filmeDTO = mock(FilmeDTO.class);

		List<FilmeDTO> filmes = new ArrayList<>();

		when(FilmeRepository.findAllByDiretor_Id(id)).thenReturn(filmes);

//		Iterable<FilmeDTO> filmesDTO = new Iterable<FilmeDTO>();
//		filmesDTO.add(filmeDTO);

		// action
		List<FilmeDTO> resultado = Lists.newArrayList(filmeService.obterFilmes(id));

		// check

		assertEquals(filmeDTO, resultado.get(0));

	}

	@Test
	public void deveCriarFilme() {
		// setup
		Filme filme = new Filme();

		//
		filmeService.criarFilme(filme);

		// check

		Mockito.verify(FilmeRepository).save(filme);

	}

	@Test
	public void deveEditarUmFilme() {
		// setup
		long id = 1L;
		String novoGenero = "XXX";
		String novoTitulo = "Bob Marley";
		Filme filmeDoBanco = criarFilme(1L);
		Filme filmeDoFront = criarFilme(1L);
		filmeDoFront.setGenero("Comedia");

		when(FilmeRepository.findById(id)).thenReturn(Optional.of(filmeDoBanco));
		when(FilmeRepository.save(filmeDoBanco)).thenReturn(filmeDoBanco);

		// action
		Filme resultado = filmeService.editarFilme(id, filmeDoFront);

		// check

		assertNotNull(resultado);
		assertEquals(novoGenero, resultado.getGenero());
		assertNotEquals(novoTitulo, resultado.getTitulo());

	}

	private Filme criarFilme(Long id) {
		Filme filme = new Filme();
		filme.setId(id);
		filme.setGenero("XXX");
		filme.setTitulo("Chicholina");
		return filme;

	}

	@Test(expected = ResponseStatusException.class)
	public void deveLancarExcecaoQuandoNaoEncontrarFilme() {
		// setup
		long id = 1L;
		Long novoId = 10L;
		Filme filmeDoFront = criarFilme(1L);
		filmeDoFront.setGenero("Comedia");

		when(FilmeRepository.findById(id)).thenReturn(Optional.empty());

		// action
		Filme resultado = filmeService.editarFilme(id, filmeDoFront);
	}

	@Test
	public void apagarFilme() {
		// setup
		long id = 1L;
		Filme filmeDoFront = criarFilme(id);
		Filme filmeDoBack = criarFilme(id);

		when(FilmeRepository.findById(id)).thenReturn(Optional.of(filmeDoBack));
		//
		filmeService.apagarFilme(filmeDoFront.getId());

		// check

		verify(FilmeRepository).delete(filmeDoFront);
		assertEquals(filmeDoFront, filmeDoBack);

	}

}

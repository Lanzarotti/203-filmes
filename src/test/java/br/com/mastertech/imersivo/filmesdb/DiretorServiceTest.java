package br.com.mastertech.imersivo.filmesdb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import br.com.mastertech.imersivo.filmesdb.model.Diretor;
import br.com.mastertech.imersivo.filmesdb.model.Filme;
import br.com.mastertech.imersivo.filmesdb.repository.DiretorRepository;
import br.com.mastertech.imersivo.filmesdb.repository.FilmeRepository;
import br.com.mastertech.imersivo.filmesdb.service.DiretorService;

@RunWith(SpringRunner.class)
//@SpringBootTest
@ContextConfiguration(classes = { DiretorService.class })
public class DiretorServiceTest {

	@Autowired
	DiretorService diretorService;
	// TODO Auto-generated constructor s

	@MockBean
	DiretorRepository diretorRepository;

	@Test
	public void deveListarTodosOsDiretores() {

		// FilmeService filmeService = new FilmeService();

		// given

		Diretor dire = new Diretor();

		List<Diretor> diretores = new ArrayList<>();
		diretores.add(dire);

		when(DiretorRepository.findAll()).thenReturn(diretores);

		// action

		Iterable<Diretor> resultado = diretorService.obterDiretores();

		// check
		Iterator<Diretor> iterador = resultado.iterator();
		Filme diretorResultado = iterador.next();

		// ArrayList<Filme> filmeList = Lists.newArrayList(filmes);

		// assertEquals(1, filmeList.size());

		assertNotNull(diretorResultado);

//		// assertNotNull(filmes);
//		int contador = 0;
//		for (Filme filme : filmes) {
//			contador++;
	}

//	@Test
//	public void deveListarTodosOsFilmesDeUmDiretor() {
//		long id = 1;
//		FilmeDTO filmeDTO = mock(FilmeDTO.class);
//
//		List<FilmeDTO> filmes = new ArrayList<>();
//
//		when(FilmeRepository.findAllByDiretor_Id(id)).thenReturn(filmes);
//
////		Iterable<FilmeDTO> filmesDTO = new Iterable<FilmeDTO>();
////		filmesDTO.add(filmeDTO);
//
//		// action
//		List<FilmeDTO> resultado = Lists.newArrayList(filmeService.obterFilmes(id));
//
//		// check
//
//		assertEquals(filmeDTO, resultado.get(0));
//
//	}

	@Test
	public void deveCriarDiretor() {
		// setup
		Diretor diretor = new Diretor();

		//
		diretorService.criarDiretor(diretor);

		// check

		Mockito.verify(DiretorRepository).save(diretor);

	}

	@Test
	public void deveEditarUmDiretor() {
		// setup
		int idade = 36;
		String novoNome = "Bob Marley";
		Filme filmeDoFront = criarFilme(1L);
		filmeDoFront.setGenero("Comedia");

		when(FilmeRepository.findById(id)).thenReturn(Optional.of(filmeDoBanco));
		when(FilmeRepository.save(filmeDoBanco)).thenReturn(filmeDoBanco);

		// action
		Filme resultado = filmeService.editarFilme(id, filmeDoFront);

		// check

		assertNotNull(resultado);
		assertEquals(novoGenero, resultado.getGenero());
		assertNotEquals(novoTitulo, resultado.getTitulo());

	}

	private Filme criarFilme(Long id) {
		Filme filme = new Filme();
		filme.setId(id);
		filme.setGenero("XXX");
		filme.setTitulo("Chicholina");
		return filme;

	}

	@Test(expected = ResponseStatusException.class)
	public void deveLancarExcecaoQuandoNaoEncontrarFilme() {
		// setup
		long id = 1L;
		Long novoId = 10L;
		Filme filmeDoFront = criarFilme(1L);
		filmeDoFront.setGenero("Comedia");

		when(FilmeRepository.findById(id)).thenReturn(Optional.empty());

		// action
		Filme resultado = filmeService.editarFilme(id, filmeDoFront);
	}

	@Test
	public void apagarFilme() {
		// setup
		long id = 1L;
		Filme filmeDoFront = criarFilme(id);
		Filme filmeDoBack = criarFilme(id);

		when(FilmeRepository.findById(id)).thenReturn(Optional.of(filmeDoBack));
		//
		filmeService.apagarFilme(filmeDoFront.getId());

		// check

		verify(FilmeRepository).delete(filmeDoFront);
		assertEquals(filmeDoFront, filmeDoBack);

	}

}
